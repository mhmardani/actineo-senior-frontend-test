module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  transform: {
    '^.+\\.vue$': 'vue-jest'
  },
  rootDir: '.',
  testMatch: ["**/?(*.)+(spec|test).[jt]s?(x)"]
}
