import { Card } from '@/types/card.type';

export default abstract class DeckService {

  public cards: Card[] = [];

  public selectedSuit = '';

  constructor(public names: Array<string>, public suits: Array<string>, public wildCards: Array<number>) {}

  shuffle(): DeckService {
    this.cards.sort(() => 0.5 - Math.random());
    return this;
  }

  getCards(numberOfCards: number): Card[] {
    return this.cards.splice(0, numberOfCards);
  }

  setSuit (suit:string): string {
    this.selectedSuit = suit
    return this.selectedSuit
  }

  getSuitColor(suit: string): string {
    switch (suit.toLowerCase()) {
      case 'hearts':
        return 'red';
      case 'diamonds':
        return 'red';
      case 'spades':
        return 'black';
      case 'clubs':
        return 'black';
      default:
        return 'black';
    }
  }

}
