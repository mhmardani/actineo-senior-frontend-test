import PockerDeckService from './pocker.deck.service';

// Here are some Jasmine 2.0 tests, though you can
// use any test runner / assertion library combo you prefer
describe('DeckService', () => {

  it('should generate a deck of cards', async () => {
    const deck = new PockerDeckService();
    await deck.generate();
    expect(deck.cards.length).toBe(52);
  });

  it('should get cards out of the generated deck', async () => {
    const deck = new PockerDeckService();
    await deck.generate();
    deck.getCards(3);
    expect(deck.cards.length).toBe(52-3);
  });

  it('should shuffle a generated deck', async () => {
    const deck = new PockerDeckService();
    await deck.generate();
    const firstCard = deck.cards[0];
    deck.shuffle();
    expect(firstCard).not.toBe(deck.cards[0]);
  });
})
