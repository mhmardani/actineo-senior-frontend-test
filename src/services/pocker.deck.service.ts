import DeckService from "./deck.service";
import ApiHelper from "./../helpers/apiCreator/index";

export default class PockerDeckService extends DeckService {
  private apiUrl = '/cardDeckGenerator.php';
  constructor(names: Array<string> = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'], suits: Array<string> = ['Hearts', 'Diamonds', 'Spades', 'Clubs'], wildCards: Array<number> = [8]) {
    super(names, suits, wildCards);
  }

  async generate(): Promise<void> {
    this.cards = (await ApiHelper.GET({url: this.apiUrl})).data
  }
}
