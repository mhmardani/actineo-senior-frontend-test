
import { Options, Vue } from 'vue-class-component';

import Card from '@/components/card/index.vue';
import PockerDeckService from '@/services/pocker.deck.service';
import { Card as CardType } from '@/types/card.type';

@Options({
  components: {
    Card
  },
})
export default class Board extends Vue {
  private currentPlayer = 1;

  public $store: any;

  public winnerIndex: number | null = 1;

  public deck: PockerDeckService = new PockerDeckService()

  public discardPile: CardType[] = [];

  public hands: Array<CardType[]> = [];

  public isSuitNeeded = false

  mounted() {
    this.restartMatch();
  }

  async restartMatch() {
    if(this.winnerIndex !== null) this.winnerIndex = null;
    await this.deck.generate();
    this.deck.shuffle();
    this.hands = [
      this.deck.getCards(7),
      this.deck.getCards(7)
    ];
    this.discardPile = this.deck.getCards(1);
  }

  play(index: number): void {
    if (!this.isPlayerTurn(index) || this.isSuitNeeded) return;

    const previousPlayer = this.currentPlayer;
    if(previousPlayer === 0 && this.isWildCard(index)){
      this.setSuit(this.deck.suits[Math.floor(Math.random() * this.deck.suits.length)])
    } else if (previousPlayer === 1 && this.isWildCard(index)) {
      this.isSuitNeeded = true
    }

    const currentPlayerHand = this.getCurrentPlayerHand();

    if (!this.isPlayable(currentPlayerHand[index])) return;

    this.discardPile.unshift(currentPlayerHand[index]);
    currentPlayerHand.splice(index, 1);
    this.currentPlayer = this.currentPlayer > 0 ? 0 : 1;
    if(this.deck.getSuitColor(this.deck.selectedSuit) !== this.deck.getSuitColor(this.discardPile[0].suit)) {
      this.deck.setSuit('')
    }
    if (this.currentPlayer === 0) {
      this.machinePlayer();
    }

    if (currentPlayerHand.length <= 0) {
      this.finishMatch();
    }
  }

  isWildCard(index: number): boolean{
    return this.deck.wildCards.indexOf(this.getCurrentPlayerHand()[index].value) !== -1
  }

  machinePlayer(): void {
    const playableCardIndex = this.findPlayableCard();
    if (playableCardIndex < 0) {
      if (!this.drawback() && this.deck.cards.length <= 0) {
        this.currentPlayer = this.currentPlayer > 0 ? 0 : 1;
        return;
      }
      return this.machinePlayer();
    }
    return this.play(playableCardIndex);
  }

  findPlayableCard(): number {
    const currentPlayerHand = this.getCurrentPlayerHand();

    let playableIndex = -1;

    currentPlayerHand.forEach((card: CardType, index: number) => {
      if (this.isPlayable(card)) playableIndex = index;
    });

    return playableIndex;
  }

  isPlayerTurn(index: number): boolean {
    return (index >= 0);
  }

  drawback(): boolean {
    const cards = this.deck.getCards(1);
    if (cards.length <= 0) {
      return false;
    }
    cards.forEach((card) => this.getCurrentPlayerHand().push(card));
    return true;
  }

  getCurrentPlayerHand(): Array<CardType> {
    return this.hands[this.currentPlayer];
  }

  finishMatch(): void {
    this.winnerIndex = (this.hands[0].length < this.hands[1].length) ? 1 : 2;
    this.$store.dispatch('incrementPlayerWinsCount', this.winnerIndex);
  }

  isPlayable(card: CardType): boolean {
    const conditions = [
      this.deck.wildCards.includes(card.value),
      card.suit === this.deck.selectedSuit,
      this.discardPile[0].value === card.value
    ];
    if(this.deck.selectedSuit){
      conditions.push(this.getCardColor(card).indexOf(this.deck.selectedSuit) !== -1)
    }else {
      conditions.push(JSON.stringify(this.getCardColor(this.discardPile[0])) === JSON.stringify(this.getCardColor(card)))
    }
    return conditions.includes(true);
  }

  getCardColor(card: CardType): Array<string> {
    const redColor = ['Hearts', 'Diamonds'];
    const blackColor = ['Spades', 'Clubs'];
    let colors = blackColor;
    if (redColor.indexOf(card.suit) >= 0) {
      colors = redColor;
    }
    return colors;
  }

  setSuitAndContinue(suit = ''): void {
    this.deck.setSuit(suit);
    this.isSuitNeeded = false;
    const currentPlayerHand = this.getCurrentPlayerHand();

    this.currentPlayer = this.currentPlayer > 0 ? 0 : 1;

    if (this.currentPlayer === 0) {
      this.machinePlayer();
    }

    if (currentPlayerHand.length <= 0) {
      this.finishMatch();
    }
  }

  setSuit(suit = ''): void {
    this.deck.setSuit(suit);
    this.isSuitNeeded = false;
  }

}

