
import { Vue } from 'vue-class-component';
import { Emit, Prop } from 'vue-property-decorator';

export default class Card extends Vue {
  @Prop() cards!: Card[];
  @Prop() hidden = true;
  @Prop() clicable = true;
  @Prop() canPlay = true;

  @Emit()
  play(index: number): number {
    if (!this.canPlay) return -1;
    return index;
  }

  getCardSuitAscii_Char(suit: string): string {
    let ascii_char = '&' + suit.toLowerCase() + ';';

    if (suit === 'Diamonds') {
      ascii_char = '&diams;';
    }

    return ascii_char;
  }
}

