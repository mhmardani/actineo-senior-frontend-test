import { createStore } from 'vuex'

export default createStore({
  modules: {
    global: {
      namespaced: true,
      state: {
        error: {
          message: ''
        },
      },
      mutations: {
        setError (state, error) {
          state.error = error
        }
      },
      actions: {
        setError ({ commit }, error) {
          commit('setError', error)
        }
      }
    },
    leaderBoard: {
      state: {
        firstPlayer: {
          winsCount: 0
        },
        secondPlayer: {
          winsCount: 0
        },
      },
      mutations: {
        incrementPlayerWinsCount(state, playerIndex) {
          const playerName = playerIndex === 0 ? 'firstPlayer' : 'secondPlayer'
          state[playerName].winsCount++
        }
      },
      actions: {
        incrementPlayerWinsCount ({ commit }, playerIndex) {
          const playerName = playerIndex === 0 ? 'firstPlayer' : 'secondPlayer'
          commit('incrementPlayerWinsCount', playerName)
        }
      },
    }
  }
})
