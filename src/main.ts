import { createApp } from 'vue'
import * as Quasar from 'quasar'
import 'quasar/dist/quasar.css'
import '@quasar/extras/material-icons/material-icons.css'
import App from './App.vue'
import router from './router'
import store from './store'

createApp(App).use(store).use(router).use(Quasar.Quasar).mount('#app')
