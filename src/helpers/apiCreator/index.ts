import Axios from 'axios';
import Store from './../../store/index'

export default class ApiHelper {
  static errorHandler(error: any): void {
    Store.dispatch('global/setError', error?.response?.data)
  }
  static apiCall(method: string, url: string, data: any): any {
    const axios: any = Axios.create({ baseURL: process.env.VUE_APP_API_BASE_URL, headers: {
      'Content-Type': 'application/json'
    }
    });
    return axios[method](url, data).catch(ApiHelper.errorHandler);
  }

  public static GET = ({ url }: any) => ApiHelper.apiCall( 'get', url, null);

  public static POST = ({ url, data }: any) => ApiHelper.apiCall( 'post', url, data);

  public static PUT = ({ url, data }: any) => ApiHelper.apiCall( 'put', url, data);

  public static PATCH = ({ url, data }: any) => ApiHelper.apiCall( 'patch', url, data);

  public static DELETE = ({ url, data }: any) => ApiHelper.apiCall( 'delete', url, data);
}
