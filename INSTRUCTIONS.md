Crazy Eights
===

A game that requires both focus and patience, in Crazy Eights, concentration is key.
The winner is the clever shark who gets rid of all his cards first! All you need is a standard 52-card deck and you're set!

## Rules

Each of the two players is dealt seven cards.
The remaining cards are placed face down in the center of the table, forming a draw pile.
The top card of the draw pile is turned face up to start the discard pile next to it.
First player adds to the discard pile by playing one card that matches the top card on the discard pile either by suit or by rank (i.e. 6, jack, ace, etc.).
A player who cannot match the top card on the discard pile by suit or rank must draw cards from the draw pile until he can play one.
When the draw pile is empty, a player who cannot add to the discard pile passes his turn.
The first player to discard all of his cards wins.

## Instructions

1. Improve the game UI/UX using a know frontend framework (preferably Materialize)
2. Pull the deck from the following json api: https://pabloleone.com/cardDeckGenerator.php
3. Remove unnecessary pages, logic and code
4. Implement the following new rule in the game:
   1. All eights are wild and can be played on any card during a player's turn.
   2. When a player discards an eight, he chooses which suit is now in play.
   3. The next player must play either a card of that suit or another eight.
5. Fix the `pocker.deck.service.spec.ts` tests and add unit unit test and one integration test
6. Move the match state to a Vuex store
7. Create a new page with game statistics using the store
8. Implement the game logic


## Requirements

1. The system can be extended with many different card decks
2. We will check the code adheres to the following principles & standards:
   1. SOLID
   2. KISS
   3. YAGNI
   4. DRY
   5. Composition over inheritance
   6. Expressive code over comments
   7. UX/UI basics
   8. Vuex
   9. Unit & integration tests
3. The game must be in working conditions