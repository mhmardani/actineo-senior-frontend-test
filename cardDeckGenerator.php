<?php
header('Content-Type: application/json; charset=utf-8');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
$names = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];
$suits = ['Hearts', 'Diamonds', 'Spades', 'Clubs'];

$randomRareError = rand(0, 100);

if($randomRareError < 10) {
    http_response_code(500);
    echo json_encode(array(
        "message" => "Internal Server Error"
    ));
}else{
    function generate($names, $suits) {
        $cards = [];
        for ($s = 0; $s < count($suits); $s++) {
            for ($n = 0; $n < count($names); $n++) {
                $card = [
                    "value" => $n + 1,
                    "name" => $names[$n],
                    "suit" => $suits[$s]
                ];
                array_push($cards, $card);
            }
        }
        return $cards;
    }

    echo json_encode(generate($names, $suits));
}